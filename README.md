# SIB WIDOCO Custom CSS

The script will download the CSS file from the SIB GitLab repository and inject it into the WIDOCO generated documentation.

The content of the `custom.css` file will be injected in `resources/css/extra.css` and content of `custom_dark.css` will be injected in `resources/css/dark.css`.

## Requirement

This script will work with a documentation generated with a **WIDOCO version >= 1.4.22** (actually tested up to version 1.4.24). Previous versions didn't include dark mode and CSS variables which these new rules rely on.

## Usage

You only need the `update_css.sh` script to update the CSS files. The script will download the css files from the SIB GitLab repository to ensure that the latest version is used.

Make the script executable `chmod +x update_css.sh`

### Update CSS

Run the script with the path to the WIDOCO generated documentation as argument.

```bash
./update_css.sh GENERATED_DOC_PATH
```

### Remove custom CSS

If you want to revert the changes made by the script, you can run the script with the `--remove` flag.

```bash
./update_css.sh GENERATED_DOC_PATH --remove
```
