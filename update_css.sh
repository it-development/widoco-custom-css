#!/bin/bash

# Directory to check for resources/extra.css
TARGET_DIR=$1

# CSS file path
CSS_FILE="$TARGET_DIR/resources/extra.css"
DARK_CSS_FILE="$TARGET_DIR/resources/dark.css"

REMOTE_CSS_URL="https://gitlab.sib.swiss/it-development/widoco-custom-css/-/raw/main/custom.css?ref_type=heads"
REMOTE_DARK_CSS_URL="https://gitlab.sib.swiss/it-development/widoco-custom-css/-/raw/main/custom_dark.css?ref_type=heads"

# Define the start and end markers
START_MARKER="/* Begin Custom CSS */"
END_MARKER="/* End Custom CSS */"

ESCAPED_START_MARKER="\/\* Begin Custom CSS \*\/"
ESCAPED_END_MARKER="\/\* End Custom CSS \*\/"

# Function to remove the injected CSS content
remove_injected_content() {
    local file="$1"

    # Check if the markers already exist in the file
    if grep -q -F "$START_MARKER" "$file" && grep -q "$ESCAPED_END_MARKER" "$file"; then
        # Remove the content between the markers
        sed -i.bak "/$ESCAPED_START_MARKER/,/$ESCAPED_END_MARKER/d" "$file"
        # Remove the backup file created by sed
        rm "$file.bak"
        echo "Injected CSS content removed successfully."
    else
        echo "No injected CSS content found."
    fi
}

# Function to inject the CSS content
inject_css_content() {
    local file="$1"
    local remote_file="$2"

    # CSS rules to inject
    local temp_file=$(mktemp)

    # Download the CSS content from the remote URL
    if curl -s -o "$temp_file" "$remote_file"; then
        echo "Downloaded CSS content from $remote_file"
    else
        echo "Failed to download CSS content from $remote_file"
        exit 1
    fi

    # Read the downloaded CSS content
    local css_rules=$(<"$temp_file")

    # Check if the CSS file exists
    if [[ -f "$file" ]]; then
        echo "Found $file"

        # Read the content of the CSS file
        local css_content=$(<"$file")

        # Check if the markers already exist in the file
        if grep -q -F "$START_MARKER" "$file"; then
            echo "found start marker"
            # Extract the part before the start marker
            local before_start=$(sed "/$ESCAPED_START_MARKER/q" "$file")

            # Combine parts with the updated CSS rules
            local new_content="$before_start
$css_rules
$END_MARKER"

            echo "Updating the existing CSS rules..."
        else
            # Append the CSS rules to the end of the file
            local new_content="$css_content
$START_MARKER
$css_rules
$END_MARKER"

            echo "Injecting new CSS rules..."
        fi

        # Write the new content back to the CSS file
        echo "$new_content" > "$file"

        echo "CSS file updated successfully."
    else
        echo "CSS file not found in $TARGET_DIR/resources."
    fi
}

# Check if the --remove argument is provided
if [[ "$2" == "--remove" ]]; then
    if [[ -f "$CSS_FILE" ]]; then
        remove_injected_content "$CSS_FILE"
        remove_injected_content "$DARK_CSS_FILE"
    else
        echo "CSS file not found in $1/resources."
    fi
    exit 0
fi

inject_css_content "$CSS_FILE" "$REMOTE_CSS_URL"
inject_css_content "$DARK_CSS_FILE" "$REMOTE_DARK_CSS_URL"

exit 0
